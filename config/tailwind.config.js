module.exports = {
  /* Use the Desy default config */
  presets: [require('desy-html/config/tailwind.config.js')],
  /* Change PurgeCSS files to add DESY AND this project's files */
  purge: {
    content: ['./node_modules/desy-html/src/**/*.html',
              './node_modules/desy-html/src/**/*.njk',
              './src/**/*.html',
              './src/**/*.njk',
              ],
    options: {
      safelist: [
                  'has-offcanvas-open',
                  'has-dialog',
                  'dialog-backdrop',
                  'dialog-backdrop.active',
                  'focus',
                  'dev',
                  'headroom',
                  'headroom--pinned',
                  'headroom--unpinned',
                  'headroom--top',
                  'headroom--not-top',
                  'headroom--bottom',
                  'headroom--not-bottom',
                  'headroom--frozen'
                  ],
    }
  },
  variants: {
    extend: {
      accessibility: ['group-hover'],
    }
  }
}
