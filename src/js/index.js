// Dependencies for dropdownComponent and listboxComponent:
// https://unpkg.com/@popperjs/core@2.4.4/dist/umd/popper.min.js
// https://unpkg.com/tippy.js@6.2.6/dist/tippy-bundle.umd.min.js


import {
  accordionComponent,
  alertComponent,
  dialogComponent,
  dropdownComponent,
  listboxComponent,
  menubarComponent,
  tableAdvancedComponent,
  tabsComponent,
  tooltipComponent,
  treeComponent
} from './desy-html.js';

var aria = aria || {};

accordionComponent(aria);
alertComponent(aria);
dialogComponent(aria);
dropdownComponent(aria);
listboxComponent(aria);
menubarComponent(aria);
tableAdvancedComponent(aria);
tabsComponent(aria);
tooltipComponent(aria);
treeComponent(aria);



const modules = document.querySelectorAll('[data-module]');
for (const item in modules) if (modules.hasOwnProperty(item)) {
  const moduleValue = modules[item].getAttribute('data-module');

  if (moduleValue == 'c-header'){
    const options = {
      // vertical offset in px before element is first unpinned
      offset : 0,
      // scroll tolerance in px before state changes
      tolerance : {
        up : 0,
        down : 0
      },
      // callback when unpinned, `this` is headroom object
      onUnpin : function() {
        // oculta todos los dropdowns que hubiese abiertos en el header
      }
    };
    const headroom = new Headroom(modules[item], options);
    headroom.init();
  }
}
